package com.example.studentregister.controller;

import com.example.studentregister.exception.ResourceNotFoundException;
import com.example.studentregister.model.User;
import com.example.studentregister.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public List<User> users() {
        return userRepository.findAll();
    }

    @PostMapping("/save")
    public void save(@RequestBody User user) {
        user.setLoginId(idGenerator());
        userRepository.save(user);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Integer id,@RequestBody User user) {
        User updateUser = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Not User By " + id));

        updateUser.setUsername(user.getUsername());
        updateUser.setPassword(user.getPassword());
        updateUser.setConfirmPassword(user.getConfirmPassword());
        updateUser.setRole(user.getRole());

        userRepository.save(updateUser);
    }

    public String idGenerator() {
        String id = "";
        List<User> list = userRepository.findAll();
        if (list == null || list.size()==0) {
            id = "USR001";
        } else {
            User lastDTO = list.get(list.size() - 1);
            int lastId = Integer.parseInt(lastDTO.getLoginId().substring(3));
            id = String.format("USR" + "%03d", lastId + 1);
        }
        return id;
    }
}
