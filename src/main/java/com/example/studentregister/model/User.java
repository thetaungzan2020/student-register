package com.example.studentregister.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@ToString
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String loginId;
    private String username;
    private String password;
    private String confirmPassword;

    @ManyToOne
    private Role role;

    public User() {
    }
}
